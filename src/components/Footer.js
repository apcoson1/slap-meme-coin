import { Flex, Text } from "@chakra-ui/react";

const Footer = () => {
  return (
    <Flex as="footer" justifyContent="center" py="5" bgColor="gray.800">
      <Text color="#798DA3" fontSize={{ base: "sm", md: "md" }}>
        Copyright {new Date().getFullYear()} SLAP. All Rights Reserved.
      </Text>
    </Flex>
  );
};

export default Footer;
