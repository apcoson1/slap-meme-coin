import { Flex } from "@chakra-ui/react";
import { BrowserRouter } from "react-router-dom";
import Header from "./components/Header";
import Footer from "./components/Footer";
import HomeScreen from "./screens/HomeScreen";

const App = () => {
  return (
    <BrowserRouter>
      <Flex direction="column" as="main">
        <Header />
        <HomeScreen />
        <Footer />
      </Flex>
    </BrowserRouter>
  );
};

export default App;
